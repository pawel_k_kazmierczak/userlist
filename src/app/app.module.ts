import { HttpClientModule } from "@angular/common/http";
import { NgModule } from "@angular/core";
import { ReactiveFormsModule } from "@angular/forms";
import { MatDialogModule, MatNativeDateModule, MAT_DIALOG_DEFAULT_OPTIONS } from "@angular/material";
import { MatDatepickerModule } from "@angular/material/datepicker";
import { BrowserModule } from "@angular/platform-browser";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { StoreModule } from "@ngrx/store";
import { StoreDevtoolsModule } from "@ngrx/store-devtools";
import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { AddUserComponent } from "./components/add-user/add-user.component";
import { DisplayCurrencyComponent } from "./components/display-currency/display-currency.component";
import { UserListComponent } from "./components/user-list/user-list.component";
import { AgePipe } from "./pipes/age.pipe";
import { DisplayCurrencyPipe } from "./pipes/display-currency.pipe";
import { AppReducers } from "./redux/index";
import { UserService } from "./services/user.service";

@NgModule({
  declarations: [AppComponent, UserListComponent, AgePipe, DisplayCurrencyComponent, DisplayCurrencyPipe, AddUserComponent],
  imports: [
    BrowserAnimationsModule,
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    MatDialogModule,
    MatDatepickerModule,
    MatNativeDateModule,
    ReactiveFormsModule,
    StoreModule.forRoot(AppReducers, {
      runtimeChecks: {
        strictStateImmutability: true,
        strictActionImmutability: true
      }
    }),
    StoreDevtoolsModule.instrument({
      maxAge: 25
    })
  ],
  providers: [{ provide: MAT_DIALOG_DEFAULT_OPTIONS, useValue: { hasBackdrop: true, title: "FD2" } }, UserService],
  bootstrap: [AppComponent],
  entryComponents: [AddUserComponent]
})
export class AppModule {
  constructor() {}
}

import { AccountModel } from "./account.model";

export interface UserModel {
  id: number;
  username: string;
  email: string;
  title: string;
  firstName: string;
  lastName: string;
  birthday: string;
  isActive: boolean;
  account?: AccountModel;
}

export const DEFAULT_CURRENCY = "PLN";

export interface AccountModel {
  id?: number;
  ownerId?: number;
  balance: number;
  currency: string;
}

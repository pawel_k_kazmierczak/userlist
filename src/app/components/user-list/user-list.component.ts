import { Component, OnInit } from "@angular/core";
import { MatDialog } from "@angular/material";
import { Observable } from "rxjs";
import { UserModel } from "../../models/user.model";
import { UserService } from "../../services/user.service";
import { AddUserComponent } from "../add-user/add-user.component";

@Component({
  selector: "fd2-user-list",
  templateUrl: "./user-list.component.html",
  styleUrls: ["./user-list.component.scss"]
})
export class UserListComponent implements OnInit {
  users$: Observable<UserModel[]>;

  constructor(private userService: UserService, public dialog: MatDialog) {}

  ngOnInit() {
    this.users$ = this.userService.getUsers();
  }

  openAddDialog() {
    this.dialog.open(AddUserComponent);
  }
}

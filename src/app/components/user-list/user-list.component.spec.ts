import { HttpClientTestingModule } from "@angular/common/http/testing";
import { async, ComponentFixture, TestBed } from "@angular/core/testing";
import { MatDialogModule } from '@angular/material';
import { of } from "rxjs";
import { AgePipe } from "../../pipes/age.pipe";
import { DisplayCurrencyPipe } from '../../pipes/display-currency.pipe';
import { UserService } from "../../services/user.service";
import { DisplayCurrencyComponent } from '../display-currency/display-currency.component';
import { UserListComponent } from "./user-list.component";

describe("UserListComponent", () => {
  let component: UserListComponent;
  let fixture: ComponentFixture<UserListComponent>;
  let userService: UserService;

  beforeEach(async(() => {
    userService = jasmine.createSpyObj("UserService", {
      getUsers: of([
        {
          id: 1,
          username: "JDoe",
          email: "jd@wodo.net",
          title: "Mr.",
          firstName: "John",
          lastName: "Doe",
          birthday: "1980-03-12T00:00:00",
          isActive: true,
          account: { balance: 0, currency: "" }
        }
      ])
    });

    TestBed.configureTestingModule({
      declarations: [UserListComponent, AgePipe, DisplayCurrencyComponent, DisplayCurrencyPipe],
      imports: [HttpClientTestingModule, MatDialogModule],
      providers: [{ provide: UserService, useValue: userService }]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
    expect(userService.getUsers).toHaveBeenCalled();
  });
});

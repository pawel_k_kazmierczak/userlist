import { Component, Input } from "@angular/core";

@Component({
  selector: "fd2-display-currency",
  templateUrl: "./display-currency.component.html",
})
export class DisplayCurrencyComponent {
  @Input()
  value: number;
  @Input()
  currency: string;
}

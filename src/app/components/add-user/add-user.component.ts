import { Component, OnInit } from "@angular/core";
import { AbstractControl, FormBuilder, FormGroup, Validators } from "@angular/forms";
import { MatDialogRef } from "@angular/material";
import { map } from 'rxjs/operators';
import { UserService } from "../../services/user.service";

@Component({
  selector: "fd2-add-user",
  templateUrl: "./add-user.component.html",
  styleUrls: ["./add-user.component.scss"]
})
export class AddUserComponent implements OnInit {
  form: FormGroup;
  constructor(private fb: FormBuilder, private dialogRef: MatDialogRef<AddUserComponent>, private userService: UserService) {}

  titleValues = ["Mr.", "Ms.", "Mrs.", "Miss"];
  errorMessage = "";

  ngOnInit() {
    this.form = this.fb.group({
      id: this.fb.control(0),
      isActive: this.fb.control(true),
      username: this.fb.control(
        "",
        [Validators.required, Validators.minLength(2), Validators.maxLength(20)],
        this.validateUsernameIsUnique.bind(this)
      ),
      email: this.fb.control("", [Validators.required, Validators.email]),
      title: this.fb.control("", [Validators.required]),
      birthday: this.fb.control(""),
      firstName: this.fb.control("", [Validators.required]),
      lastName: this.fb.control("", [Validators.required])
    });
  }

  validateUsernameIsUnique(control: AbstractControl) {
    return this.userService.checkUsernameIsUniqueness(control.value).pipe(
      map(res => {
        return res ? null : { usernameUniquenes: true };
      })
    );
  }

  saveUser() {
    if (this.form.valid) {
      const post = this.form.value;

      this.userService.saveUser(post).subscribe(
        () => {
          this.dialogRef.close();
        },
        error => {
          this.errorMessage = "Some error happened, please try later";
        }
      );
    }
  }

  cleanMessage() {
    this.errorMessage = "";
  }

  get username() {
    return this.form.get("username");
  }
  get email() {
    return this.form.get("email");
  }
  get title() {
    return this.form.get("title");
  }
  get firstName() {
    return this.form.get("firstName");
  }
  get lastName() {
    return this.form.get("lastName");
  }
}

import { createReducer, on } from "@ngrx/store";
import { initialState } from "../initialState";
import { setAccounts } from "./actions";

const _accountReducer = createReducer(
  initialState,
  on(setAccounts, (state, params) => {
    const result = {};

    for (let account of params.accounts) {
      result[account.ownerId] = account;
    }

    return result;
  })
);

export function accountsReducer(state, action) {
  return _accountReducer(state, action);
}

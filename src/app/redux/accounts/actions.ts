import { createAction, props } from "@ngrx/store";
import { AccountModel } from '../../models/account.model';

export const setAccounts = createAction("[Account] setAccounts", props<{ accounts: AccountModel[] }>());

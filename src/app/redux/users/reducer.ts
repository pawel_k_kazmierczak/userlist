import { createReducer, on } from "@ngrx/store";
import { initialState } from "../initialState";
import { addUser, setUsers } from "./actions";

const _userReducer = createReducer(
  initialState.users,
  on(setUsers, (state, params) => {
    const result = {};

    for (let user of params.users) {
      result[user.id] = user;
    }

    return result;
  }),
  on(addUser, (state, params) => {
    const result = { ...state };

    result[params.user.id] = params.user;

    return result;
  })
);

export function userReducer(state, action) {
  return _userReducer(state, action);
}

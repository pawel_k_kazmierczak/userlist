import { AppState } from "..";
import { DEFAULT_CURRENCY } from "../../models/account.model";
import { UserModel } from "../../models/user.model";

export const selectUsers = (state: AppState): UserModel[] => {
  const result = [];

  for (const id in state.users) {
    const user = { ...state.users[id] };

    if (state.accounts[id] !== undefined) {
      user.account = { ...state.accounts[id] };
    } else {
      user.account = { balance: 0, currency: DEFAULT_CURRENCY };
    }

    result.push(user);
  }

  result.sort(sortUser);

  return result;
};

const sortUser = (a: UserModel, b: UserModel): number => {
  if (a.isActive !== b.isActive) {
    return a.isActive ? -1 : 1;
  }
  if (a.username > b.username) {
    return 1;
  }
  if (b.username > a.username) {
    return -1;
  }
  return 0;
};

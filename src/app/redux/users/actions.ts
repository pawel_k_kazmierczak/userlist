import { createAction, props } from "@ngrx/store";
import { UserModel } from "../../models/user.model";

export const setUsers = createAction("[User] setUsers", props<{ users: UserModel[] }>());
export const addUser = createAction("[User] addUser", props<{ user: UserModel }>());

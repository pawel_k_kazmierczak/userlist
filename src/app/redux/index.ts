import { ActionReducerMap } from "@ngrx/store";
import { AccountModel } from "../models/account.model";
import { UserModel } from "../models/user.model";
import { accountsReducer } from "./accounts/reducer";
import { userReducer as usersReducer } from "./users/reducer";

export interface AppState {
  users: { [key: number]: UserModel };
  accounts: { [key: number]: AccountModel };
}

export const AppReducers: ActionReducerMap<AppState> = {
  users: usersReducer,
  accounts: accountsReducer
};

import { Pipe, PipeTransform } from "@angular/core";
import * as moment from "moment";
@Pipe({
  name: "age"
})
export class AgePipe implements PipeTransform {
  transform(value: string | Date | moment.Moment): number {
    if (value) {
      return moment().diff(value, "years");
    }
  }
}

import { Pipe, PipeTransform } from "@angular/core";

@Pipe({
  name: "displayCurrency"
})
export class DisplayCurrencyPipe implements PipeTransform {
  transform(value: number): string {
    return Intl.NumberFormat('fr-fr', {minimumFractionDigits: 2}).format(value)
  }
}

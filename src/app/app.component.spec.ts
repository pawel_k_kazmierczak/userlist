import { HttpClientTestingModule } from "@angular/common/http/testing";
import { Component } from "@angular/core";
import { async, ComponentFixture, TestBed } from "@angular/core/testing";
import { BrowserModule, By } from "@angular/platform-browser";
import { RouterTestingModule } from "@angular/router/testing";
import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";

describe("AppComponent", () => {
  let fixture: ComponentFixture<AppComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [RouterTestingModule, BrowserModule, AppRoutingModule, HttpClientTestingModule],
      declarations: [AppComponent, UserListComponentMock]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AppComponent);
  });

  it("should create the app", () => {
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  });

  it("should render child component", () => {
    const childComponent = fixture.debugElement.query(By.directive(UserListComponentMock));
    expect(childComponent).toBeTruthy();
  });
});

@Component({
  selector: "fd2-user-list",
  template: "<div>some value</div>"
})
export class UserListComponentMock {}

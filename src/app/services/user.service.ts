import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Store } from "@ngrx/store";
import { Observable } from "rxjs";
import { take, tap } from "rxjs/operators";
import { environment } from "../../environments/environment";
import { AccountModel } from "../models/account.model";
import { UserModel } from "../models/user.model";
import { AppState } from "../redux";
import { setAccounts } from "../redux/accounts/actions";
import { addUser, setUsers } from "../redux/users/actions";
import { selectUsers } from "../redux/users/selector";

@Injectable()
export class UserService {
  protected users$;

  constructor(private http: HttpClient, private store: Store<AppState>) {}

  getUsers(): Observable<UserModel[]> {
    if (!this.users$) {
      this.users$ = this.store.select(selectUsers);

      this.http
        .get<UserModel[]>(environment.apiBaseUrl + "users")
        .pipe(take(1))
        .subscribe(users => {
          this.store.dispatch(setUsers({ users }));
        });
      this.http
        .get<AccountModel[]>(environment.apiBaseUrl + "accounts")
        .pipe(take(1))
        .subscribe(accounts => {
          this.store.dispatch(setAccounts({ accounts }));
        });
    }
    return this.users$;
  }

  saveUser(data): Observable<UserModel> {
    return this.http.post<UserModel>(environment.apiBaseUrl + "users", data).pipe(
      tap(user => {
        this.store.dispatch(addUser({ user }));
      })
    );
  }

  checkUsernameIsUniqueness(username: string): Observable<boolean> {
    return this.http.get<boolean>(environment.apiBaseUrl + "users/" + username + "/uniqueness");
  }
}
